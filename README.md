University of Dayton

Department of Computer Science

CPS 490 - Capstone I, Fall 2019

Instructor: Dr. Phu Phung

## Capstone I Project 

# The Messenger

# Team members

1. Phu Phung, <phu@udayton.edu>
2. Joe J, <nobody@udayton.edu>

# System Analysis

_(Start from Sprint 0, keep updating)_

## User Requirements

List user requirements of the project that your team will develop into use cases in later steps

  * Users can type text and send to a single receiver
  * Users can type text and send to a group 
  * Users can register to the system 

## Use cases

### Overview diagram

![Use Case Diagram](diagrams/use-case.png "Use Case Diagram")

### 1. Register an account

Actor: Potential User

## git branch

Just a testing for git branch
